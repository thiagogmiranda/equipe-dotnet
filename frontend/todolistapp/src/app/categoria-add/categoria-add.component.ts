import { Component, OnInit } from '@angular/core';
import { CategoriaService } from '../services/categoria.service';
import { Categoria, Lista, Tarefa } from '../models/models';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-categoria-add',
  templateUrl: './categoria-add.component.html',
  styleUrls: ['./categoria-add.component.css']
})
export class CategoriaAddComponent implements OnInit {
  categoriaForm: FormGroup;
  error: string = '';

  constructor(
    public fb: FormBuilder,
    public categoriaService: CategoriaService,
    private router: Router,
    private route: ActivatedRoute) {

      this.categoriaForm = this.fb.group({
        nome: ['', Validators.required]
      });
    }

  ngOnInit(): void {
    
  }

  public handleError = (controlName: string, errorName: string) => {
    return this.categoriaForm.controls[controlName].hasError(errorName);
  } 

  submitForm() {
    if(this.categoriaForm.valid) {
      this.categoriaService.addCategoria(this.categoriaForm.value)
      .pipe(first())
      .subscribe({
        next: () => {
          this.router.navigate(['/categorias']);
        },
        error: error => {
          this.error = error;
        }
      });
    }
  }

}
