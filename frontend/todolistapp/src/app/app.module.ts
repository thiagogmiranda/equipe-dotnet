import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularMaterialModule } from './material.module';

import { FlexLayoutModule } from '@angular/flex-layout';

import { ListaComponent } from './lista/lista.component';
import { ListaAddComponent } from './lista-add/lista-add.component';
import { ListaDetailComponent } from './lista-detail/lista-detail.component';
import { ListaEditComponent } from './lista-edit/lista-edit.component';
import { LogInComponent } from './log-in/log-in.component';
import { RegisterComponent } from './register/register.component';

import { JwtInterceptor } from './helpers/jwt.interceptor';
import { TarefaAddComponent } from './tarefa-add/tarefa-add.component';
import { TarefaEditComponent } from './tarefa-edit/tarefa-edit.component';
import { CategoriaComponent } from './categoria/categoria.component';
import { CategoriaAddComponent } from './categoria-add/categoria-add.component';
import { CategoriaEditComponent } from './categoria-edit/categoria-edit.component';

@NgModule({ 
  declarations: [
    AppComponent,
    ListaComponent,
    ListaAddComponent,
    ListaDetailComponent,
    ListaEditComponent,
    LogInComponent,
    RegisterComponent,
    TarefaAddComponent,
    TarefaEditComponent,
    CategoriaComponent,
    CategoriaAddComponent,
    CategoriaEditComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    AngularMaterialModule,
    FlexLayoutModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true }
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
