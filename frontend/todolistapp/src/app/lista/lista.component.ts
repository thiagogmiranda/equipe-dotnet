import { Component, OnInit } from '@angular/core';
import { ListaService } from '../services/lista.service';
import { CategoriaService } from '../services/categoria.service';
import { Categoria, Lista } from '../models/models';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup } from "@angular/forms";
import { MatFormField, MatFormFieldControl } from '@angular/material/form-field';

@Component({
  selector: 'app-lista',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.css']
})
export class ListaComponent implements OnInit {
  displayedColumns: string[] = ['nome', 'categoria', 'concluida', 'opcoes'];
  dataSource: Lista[] = [];
  categorias: Categoria[] = [];
  filtroForm: FormGroup;

  constructor(
    public fb: FormBuilder,
    private rest: ListaService,
    private categoriaService: CategoriaService,
    private router: Router) { 
      this.filtroForm = this.fb.group({
        nome: [''],
        categoriaId: [0],
      });
    }

  ngOnInit(): void {
    this.categoriaService.getCategorias().subscribe((resp: any) => {
      this.categorias = [ { id: 0, nome: 'Todas'} ].concat(resp);
    });
    
    this.getListas();
  }

  getListas(): void {
    var nome = this.filtroForm.controls.nome.value || '';
    var categoriaId = this.filtroForm.value.categoriaId || 0; 

    this.rest.getListas(nome, categoriaId).subscribe((resp: any) => {
      this.dataSource = resp;
    });
  }

  delete(id: number): void {
    this.rest.deleteLista(id)
      .subscribe(() => {
        this.getListas();
      }, (err) => {
        console.log(err);
      }
      );
  }

  concluir(id: number): void {
    this.rest.concluirLista(id)
      .subscribe(() => {
        this.getListas();
      }, (err) => {
        console.log(err);
      }
      );
  }
}