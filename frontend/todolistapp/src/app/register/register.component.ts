import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Validators, FormControl } from '@angular/forms';
import { first } from 'rxjs/operators';
import { UsuarioService } from '../services/usuario.service';
import { Usuario } from '../models/models';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})

export class RegisterComponent implements OnInit {
  nomeFormControl = new FormControl('', [
    Validators.required,
  ]);
  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email
  ]);
  senhaFormControl = new FormControl('', [
    Validators.required
  ]);

  error: string = '';

  constructor(
    private usuarioService: UsuarioService,
    private route: ActivatedRoute,
    private router: Router,) { }

  ngOnInit() {
  }

  onSubmit() {
    if (this.emailFormControl.invalid || this.emailFormControl.invalid || this.senhaFormControl.invalid) {
      return;
    }

    var usuario = new Usuario(0, this.nomeFormControl.value, this.emailFormControl.value, this.senhaFormControl.value);

    this.usuarioService.addUsuario(usuario)
      .pipe(first())
      .subscribe({
        next: () => {
          this.router.navigate(['/login']);
        },
        error: error => {
          this.error = error;
        }
      });
  }
}