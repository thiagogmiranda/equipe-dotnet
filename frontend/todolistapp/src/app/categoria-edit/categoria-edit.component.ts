import { Component, OnInit } from '@angular/core';
import { CategoriaService } from '../services/categoria.service';
import { Categoria, Lista, Tarefa } from '../models/models';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-categoria-edit',
  templateUrl: './categoria-edit.component.html',
  styleUrls: ['./categoria-edit.component.css']
})
export class CategoriaEditComponent implements OnInit {
  categoriaForm: FormGroup;
  error: string = '';
  categoriaId = 0;

  constructor(
    public fb: FormBuilder,
    public categoriaService: CategoriaService,
    private router: Router,
    private route: ActivatedRoute) {

      this.categoriaId = parseInt(this.route.snapshot.paramMap.get('id') || '0');

      this.categoriaForm = this.fb.group({
        id: ['', Validators.required],
        nome: ['', Validators.required]
      });
    }

  ngOnInit(): void {
    this.categoriaService.getCategoria(this.categoriaId).subscribe((resp: any) => {

      this.categoriaForm = this.fb.group({
        id: [resp.id, Validators.required],
        nome: [resp.nome, Validators.required]
      });
    });
  }

  public handleError = (controlName: string, errorName: string) => {
    return this.categoriaForm.controls[controlName].hasError(errorName);
  } 

  submitForm() {
    if(this.categoriaForm.valid) {
      this.categoriaService.updateCategoria(this.categoriaId, this.categoriaForm.value)
      .pipe(first())
      .subscribe({
        next: () => {
          this.router.navigate(['/categorias']);
        },
        error: error => {
          this.error = error;
        }
      });
    }
  }
}
