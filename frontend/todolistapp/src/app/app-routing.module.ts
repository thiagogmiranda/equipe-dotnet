import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from './helpers/auth.guard';

import { LogInComponent } from './log-in/log-in.component';
import { RegisterComponent } from './register/register.component';

import { ListaComponent } from './lista/lista.component';
import { ListaDetailComponent } from './lista-detail/lista-detail.component';
import { ListaAddComponent } from './lista-add/lista-add.component';
import { ListaEditComponent } from './lista-edit/lista-edit.component';

import { TarefaAddComponent } from './tarefa-add/tarefa-add.component';
import { TarefaEditComponent } from './tarefa-edit/tarefa-edit.component';

import { CategoriaComponent } from './categoria/categoria.component';
import { CategoriaAddComponent } from './categoria-add/categoria-add.component';
import { CategoriaEditComponent } from './categoria-edit/categoria-edit.component';

const routes: Routes = [
  { path: 'login', component: LogInComponent },
  { path: 'register', component: RegisterComponent },
  {
    path: '',
    component: ListaComponent,
    data: { title: 'Listas' },
    canActivate: [AuthGuard]
  },
  {
    path: 'lista-details/:id',
    component: ListaDetailComponent,
    data: { title: 'Detalhes da lista' },
    canActivate: [AuthGuard]
  },
  {
    path: 'lista-add',
    component: ListaAddComponent,
    data: { title: 'Nova lista' },
    canActivate: [AuthGuard]
  },
  {
    path: 'lista-edit/:id',
    component: ListaEditComponent,
    data: { title: 'Editar lista' },
    canActivate: [AuthGuard]
  },
  {
    path: 'listas/:id/tarefa-add',
    component: TarefaAddComponent,
    data: { title: 'Nova tarefa'},
    canActivate: [AuthGuard]
  },
  {
    path: 'tarefa-edit/:id',
    component: TarefaEditComponent,
    data: { title: 'Editar tarefa'},
    canActivate: [AuthGuard]
  },
  {
    path: 'categorias',
    component: CategoriaComponent,
    data: { title: 'Listar categorias'},
    canActivate: [AuthGuard]
  },
  {
    path: 'categoria-add',
    component: CategoriaAddComponent,
    data: { title: 'Criar categoria'},
    canActivate: [AuthGuard]
  },
  {
    path: 'categoria-edit/:id',
    component: CategoriaEditComponent,
    data: { title: 'Editar categoria'},
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
