import { Component, OnInit, ViewChild } from '@angular/core';
import { ListaService } from '../services/lista.service';
import { CategoriaService } from '../services/categoria.service';
import { Categoria, Lista } from '../models/models';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-lista-add',
  templateUrl: './lista-add.component.html',
  styleUrls: ['./lista-add.component.css']
})
export class ListaAddComponent implements OnInit {
  listaForm: FormGroup;
  categorias: Categoria[] = [];
  error: string = '';

  constructor(
    public fb: FormBuilder,
    public listaService: ListaService,
    public categoriaService: CategoriaService,
    private router: Router,
    private route: ActivatedRoute) {

      this.listaForm = this.fb.group({
        nome: ['', Validators.required],
        categoriaId: ['', Validators.required],
      });
    }

  ngOnInit(): void {
    this.categoriaService.getCategorias().subscribe((resp: any) => {
      this.categorias = resp;
    });
  }

  public handleError = (controlName: string, errorName: string) => {
    return this.listaForm.controls[controlName].hasError(errorName);
  } 

  submitForm() {
    if(this.listaForm.valid) {
      this.listaService.addLista(this.listaForm.value)
      .pipe(first())
      .subscribe({
        next: () => {
          this.router.navigate(['/']);
        },
        error: error => {
          this.error = error;
        }
      });
    }
  }
}
