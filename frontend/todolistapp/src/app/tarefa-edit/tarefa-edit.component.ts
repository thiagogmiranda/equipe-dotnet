import { Component, OnInit } from '@angular/core';
import { TarefaService } from '../services/tarefa.service';
import { Categoria, Lista, Tarefa } from '../models/models';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { first } from 'rxjs/operators';
import { AuthenticationService } from '../services/authentication.service';
import { Usuario, NullableUsuario } from '../models/models';

@Component({
  selector: 'app-tarefa-edit',
  templateUrl: './tarefa-edit.component.html',
  styleUrls: ['./tarefa-edit.component.css']
})
export class TarefaEditComponent implements OnInit {

  tarefaForm: FormGroup;
  error: string = '';
  tarefaId = 0;
  listaId = 0;
  currentUser: NullableUsuario | undefined;

  constructor(
    public fb: FormBuilder,
    public tarefaService: TarefaService,
    private authenticationService: AuthenticationService,
    private router: Router,
    private route: ActivatedRoute) {

      this.authenticationService.currentUser.subscribe(x => this.currentUser = x);

      this.tarefaId = parseInt(this.route.snapshot.paramMap.get('id') || '0');

      this.tarefaForm = this.fb.group({
        nome: ['', Validators.required],
        id: [this.tarefaId, Validators.required],
        listaId: ['', Validators.required],
        usuarioId: ['', Validators.required],
      });
    }

  ngOnInit(): void {
    this.tarefaService.getTarefa(this.tarefaId).subscribe((resp: any) => {
      this.listaId = resp.listaId;

      this.tarefaForm = this.fb.group({
        nome: [resp.nome, Validators.required],
        id: [this.tarefaId, Validators.required],
        listaId: [resp.listaId, Validators.required],
        usuarioId: [resp.usuarioId, Validators.required],
      });
    });
  }

  public handleError = (controlName: string, errorName: string) => {
    return this.tarefaForm.controls[controlName].hasError(errorName);
  } 

  submitForm() {
    if(this.tarefaForm.valid) {
      this.tarefaService.updateTarefa(this.tarefaId, this.tarefaForm.value)
      .pipe(first())
      .subscribe({
        next: () => {
          this.router.navigate(['/lista-details/' + this.listaId]);
        },
        error: error => {
          this.error = error;
        }
      });
    }
  }

}
