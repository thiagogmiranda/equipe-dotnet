import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/internal/operators';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { environment } from '../../environments/environment';
import { Lista } from '../models/models';

const endpoint = `${environment.apiUrl}/listas`

@Injectable({
  providedIn: 'root'
})
export class ListaService {

  constructor(private http: HttpClient) { }

  private handleError(error: HttpErrorResponse): any {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    return throwError(
      'Something bad happened; please try again later.');
  }

  getListas(nome: string, categoriaId: number): Observable<any> {
    return this.http.get<Lista>(endpoint + `?nome=${nome}&idCategoria=${categoriaId}`).pipe(catchError(this.handleError));
  }

  getLista(id: number): Observable<any> {
    return this.http.get<Lista>(endpoint + '/' + id).pipe(
      catchError(this.handleError)
    );
  }

  addLista(lista: any): Observable<any> {
    return this.http.post(endpoint, lista).pipe(
      catchError(this.handleError)
    );
  }

  updateLista(id: number, lista: any): Observable<any> {
    return this.http.put<Lista>(endpoint + '/' + id, lista).pipe(
      catchError(this.handleError)
    );
  }

  concluirLista(id: number): Observable<any> {
    return this.http.put(endpoint + '/concluir/' + id, { id }).pipe(
      catchError(this.handleError)
    );
  }

  deleteLista(id: number): Observable<any> {
    return this.http.delete<Lista>(endpoint + '/' + id).pipe(
      catchError(this.handleError)
    );
  }
}
