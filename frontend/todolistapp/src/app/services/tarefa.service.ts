import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/internal/operators';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { environment } from '../../environments/environment';
import { Tarefa } from '../models/models';

const endpoint = `${environment.apiUrl}/tarefas`

@Injectable({
  providedIn: 'root'
})
export class TarefaService {

  constructor(private http: HttpClient) { }

  private handleError(error: HttpErrorResponse): any {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    return throwError(
      'Something bad happened; please try again later.');
  }

  getTarefas(idLista: number): Observable<any> {
    return this.http.get<Tarefa>(endpoint + '/lista/' + idLista).pipe(catchError(this.handleError));
  }

  getTarefa(id: number): Observable<any> {
    return this.http.get<Tarefa>(endpoint + '/' + id).pipe(
      catchError(this.handleError)
    );
  }

  addTarefa(obj: any): Observable<any> {
    return this.http.post(endpoint, obj).pipe(
      catchError(this.handleError)
    );
  }

  updateTarefa(id: number, obj: any): Observable<any> {
    return this.http.put<Tarefa>(endpoint + '/' + id, obj).pipe(
      catchError(this.handleError)
    );
  }

  concluirTarefa(id: number): Observable<any> {
    return this.http.put(endpoint + '/concluir/' + id, { id }).pipe(
      catchError(this.handleError)
    );
  }

  deleteTarefa(id: number): Observable<any> {
    return this.http.delete<Tarefa>(endpoint + '/' + id).pipe(
      catchError(this.handleError)
    );
  }
}
