import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/internal/operators';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { environment } from '../../environments/environment';
import { Categoria } from '../models/models';

const endpoint = `${environment.apiUrl}/categorias`

@Injectable({
  providedIn: 'root'
})
export class CategoriaService {

  constructor(private http: HttpClient) { }

  private handleError(error: HttpErrorResponse): any {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    return throwError(
      'Something bad happened; please try again later.');
  }

  getCategorias(): Observable<any> {
    return this.http.get<Categoria>(endpoint).pipe(catchError(this.handleError));
  }

  getCategoria(id: number): Observable<any> {
    return this.http.get<Categoria>(endpoint + '/' + id).pipe(
      catchError(this.handleError)
    );
  }

  addCategoria(categoria: any): Observable<any> {
    return this.http.post(endpoint, categoria).pipe(
      catchError(this.handleError)
    );
  }

  updateCategoria(id: number, categoria: Categoria): Observable<any> {
    return this.http.put<Categoria>(endpoint + '/' + id, categoria).pipe(
      catchError(this.handleError)
    );
  }

  deleteCategoria(id: number): Observable<any> {
    return this.http.delete<Categoria>(endpoint + '/' + id).pipe(
      catchError(this.handleError)
    );
  }
}
