export type NullableUsuario = Usuario | null;

export class Usuario {
    id: number;
    nome: string;
    email: string;
    senha: string;
    token: string | undefined;

    constructor(id: number, nome: string, email: string, senha: string) {
        this.id = id;
        this.nome = nome;
        this.email = email;
        this.senha = senha;
    }

    isValid() {
        return this.id > 0 && this.nome != '' && this.email !== '' && this.token;
    }
}

export class Categoria {
    id: number;
    nome: string;

    constructor(id: number, nome: string) {
        this.id = id;
        this.nome = nome;        
    }
}

export class Lista {
    id: number;
    nome: string;
    concluida: boolean = false;
    categoria: Categoria;

    constructor(id: number, nome: string, categoria: Categoria) {
        this.id = id;
        this.nome = nome;
        this.categoria = categoria;
    }
}

export class Tarefa {
    id: number;
    nome: string;
    concluida: boolean = false;
    listaId: number;
    usuarioId: number;

    constructor(id: number, nome: string, listaId: number, usuarioId: number) {
        this.id = id;
        this.nome = nome;        
        this.listaId = listaId;
        this.usuarioId = usuarioId;
    }
}
