import { Component, OnInit } from '@angular/core';
import { TarefaService } from '../services/tarefa.service';
import { Categoria, Lista, Tarefa } from '../models/models';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { first } from 'rxjs/operators';
import { AuthenticationService } from '../services/authentication.service';
import { Usuario, NullableUsuario } from '../models/models';

@Component({
  selector: 'app-tarefa-add',
  templateUrl: './tarefa-add.component.html',
  styleUrls: ['./tarefa-add.component.css']
})
export class TarefaAddComponent implements OnInit {
  tarefaForm: FormGroup;
  error: string = '';
  listaId = 0;
  usuarioId = 0;
  currentUser: NullableUsuario | undefined;

  constructor(
    public fb: FormBuilder,
    public tarefaService: TarefaService,
    private authenticationService: AuthenticationService,
    private router: Router,
    private route: ActivatedRoute) {

      this.authenticationService.currentUser.subscribe(x => this.currentUser = x);

      this.listaId = parseInt(this.route.snapshot.paramMap.get('id') || '0');
      this.usuarioId = this.currentUser?.id || 0;

      this.tarefaForm = this.fb.group({
        nome: ['', Validators.required],
        listaId: [this.listaId, Validators.required],
        usuarioId: [this.usuarioId, Validators.required],
      });
    }

  ngOnInit(): void {
    
  }

  public handleError = (controlName: string, errorName: string) => {
    return this.tarefaForm.controls[controlName].hasError(errorName);
  } 

  submitForm() {
    if(this.tarefaForm.valid) {
      this.tarefaService.addTarefa(this.tarefaForm.value)
      .pipe(first())
      .subscribe({
        next: () => {
          this.router.navigate(['/lista-details/' + this.listaId]);
        },
        error: error => {
          this.error = error;
        }
      });
    }
  }

}
