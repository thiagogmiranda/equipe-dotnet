import { Component, OnInit } from '@angular/core';
import { ListaService } from '../services/lista.service';
import { CategoriaService } from '../services/categoria.service';
import { TarefaService } from '../services/tarefa.service';
import { Categoria, Lista, Tarefa } from '../models/models';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder } from "@angular/forms";

@Component({
  selector: 'app-lista-detail',
  templateUrl: './lista-detail.component.html',
  styleUrls: ['./lista-detail.component.css']
})
export class ListaDetailComponent implements OnInit {

  idLista: number = 0;
  lista: Lista = { id: 0, nome: '', concluida: false, categoria: { id: 0, nome: '' } };
  tarefas: Tarefa[] = [];
  displayedColumns: string[] = ['nome', 'usuario', 'concluida', 'opcoes'];

  constructor(
    public fb: FormBuilder,
    public listaService: ListaService,
    public categoriaService: CategoriaService,
    public tarefaService: TarefaService,
    private route: ActivatedRoute,
    private router: Router) {

    this.idLista = parseInt(this.route.snapshot.paramMap.get('id') || '0');
  }

  ngOnInit(): void {
    this.listaService.getLista(this.idLista).subscribe((resp: any) => {
      this.lista = resp;

      this.categoriaService.getCategoria(resp.categoriaId).subscribe((resp: any) => {
        this.lista.categoria = resp;

        this.getTarefas();
      });
    });
  }

  getTarefas() {
    this.tarefaService.getTarefas(this.lista.id).subscribe((resp: any) => {
      this.tarefas = resp;
    });
  }

  delete(id: number): void {
    this.listaService.deleteLista(id)
      .subscribe(() => {
        this.router.navigate(['/']);
      }, (err) => {
        console.log(err);
      }
      );
  }

  deleteTarefa(id: number): void {
    this.tarefaService.deleteTarefa(id)
      .subscribe(() => {
        this.getTarefas();
      }, (err) => {
        console.log(err);
      }
      );
  }

  concluirTarefa(id: number): void {
    this.tarefaService.concluirTarefa(id)
      .subscribe(() => {
        this.getTarefas();
      }, (err) => {
        console.log(err);
      }
      );
  }
}
