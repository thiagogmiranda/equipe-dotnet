import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaDetailComponent } from './lista-detail.component';

describe('ListaDetailComponent', () => {
  let component: ListaDetailComponent;
  let fixture: ComponentFixture<ListaDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListaDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
