import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaEditComponent } from './lista-edit.component';

describe('ListaEditComponent', () => {
  let component: ListaEditComponent;
  let fixture: ComponentFixture<ListaEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListaEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
