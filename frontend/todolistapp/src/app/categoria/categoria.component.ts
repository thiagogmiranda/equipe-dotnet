import { Component, OnInit } from '@angular/core';
import { CategoriaService } from '../services/categoria.service';
import { Categoria } from '../models/models';
import { Router } from '@angular/router';

@Component({
  selector: 'app-categoria',
  templateUrl: './categoria.component.html',
  styleUrls: ['./categoria.component.css']
})
export class CategoriaComponent implements OnInit {
  displayedColumns: string[] = ['nome', 'opcoes'];
  dataSource: Categoria[] = [];

  constructor(
    public rest: CategoriaService,
    private router: Router) { }

  ngOnInit(): void {
    this.getListas();
  }

  getListas(): void {
    this.rest.getCategorias().subscribe((resp: any) => {
      this.dataSource = resp;
    });
  }

  // delete(id: number): void {
  //   this.rest.deleteCategoria(id)
  //     .subscribe(() => {
  //         this.getListas();
  //       }, (err) => {
  //         console.log(err);
  //       }
  //     );
  // }

}
