﻿using TodoListApp.Domain.Entities;
using TodoListApp.Domain.Interfaces.Repositories;
using TodoListApp.Infra.Data.Context;

namespace TodoListApp.Infra.Data.Repositories
{
	public class CategoriaRepository : RepositoryBase<Categoria>, ICategoriaRepository
	{
		public CategoriaRepository(TodoListAppContext db)
			: base(db)
		{
		}
	}
}
