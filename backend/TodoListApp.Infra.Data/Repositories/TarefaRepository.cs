﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using TodoListApp.Domain.Entities;
using TodoListApp.Domain.Interfaces.Repositories;
using TodoListApp.Infra.Data.Context;

namespace TodoListApp.Infra.Data.Repositories
{
	public class TarefaRepository : RepositoryBase<Tarefa>, ITarefaRepository
	{
		public TarefaRepository(TodoListAppContext db) : base(db)
		{
		}

		public IEnumerable<Tarefa> ObterPorLista(int idLista)
		{
			return Db.Tarefas.Include(t => t.Usuario).Where(t => t.ListaId == idLista).ToList();
		}
	}
}
