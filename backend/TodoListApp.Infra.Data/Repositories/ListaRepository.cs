﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using TodoListApp.Domain.Entities;
using TodoListApp.Domain.Interfaces.Repositories;
using TodoListApp.Infra.Data.Context;

namespace TodoListApp.Infra.Data.Repositories
{
	public class ListaRepository : RepositoryBase<Lista>, IListaRepository
	{
		public ListaRepository(TodoListAppContext db) : base(db)
		{
		}

		public IEnumerable<Lista> ListarPorNomeECategoria(string nome, int idCategoria)
		{
			var query = Db.Listas
				.Include(l => l.Categoria)
				.AsQueryable();

			if (!string.IsNullOrWhiteSpace(nome))
			{
				query = query.Where(l => l.Nome.StartsWith(nome));
			}
			if (idCategoria > 0)
			{
				query = query.Where(l => l.CategoriaId == idCategoria);
			}

			return query.ToList();
		}
	}
}
