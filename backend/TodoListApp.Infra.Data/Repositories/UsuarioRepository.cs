﻿using System.Linq;
using TodoListApp.Domain.Entities;
using TodoListApp.Domain.Interfaces.Repositories;
using TodoListApp.Infra.Data.Context;

namespace TodoListApp.Infra.Data.Repositories
{
	public class UsuarioRepository : RepositoryBase<Usuario>, IUsuarioRepository
	{
		public UsuarioRepository(TodoListAppContext db)
			: base(db)
		{

		}

		public Usuario ObterParaLogin(string email, string senha)
		{
			return Db.Usuarios.Where(u => u.Email == email && u.Senha == senha).FirstOrDefault();
		}
	}
}
