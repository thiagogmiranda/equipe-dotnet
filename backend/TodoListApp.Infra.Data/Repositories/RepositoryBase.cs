﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using TodoListApp.Domain.Interfaces.Repositories;
using TodoListApp.Infra.Data.Context;

namespace TodoListApp.Infra.Data.Repositories
{
	public class RepositoryBase<TEntidade> : IDisposable, IRepositoryBase<TEntidade> where TEntidade : class
	{
		protected readonly TodoListAppContext Db;

		public RepositoryBase(TodoListAppContext db)
		{
			Db = db;
		}

		public void Adicionar(TEntidade entidade)
		{
			Db.Set<TEntidade>().Add(entidade);
			Db.SaveChanges();
		}

		public void Atualizar(TEntidade entidade)
		{
			Db.Entry(entidade).State = EntityState.Modified;
			Db.SaveChanges();
		}

		public void Dispose()
		{
			//throw new NotImplementedException();
		}

		public TEntidade ObterPorId(int id)
		{
			var entity = Db.Set<TEntidade>().Find(id);
			Db.Entry(entity).State = EntityState.Detached;
			return entity;
		}

		public IEnumerable<TEntidade> ObterTodos()
		{
			return Db.Set<TEntidade>().ToList();
		}

		public void Remover(int id)
		{
			var obj = ObterPorId(id);
			Db.Set<TEntidade>().Remove(obj);
			Db.SaveChanges();
		}
	}
}
