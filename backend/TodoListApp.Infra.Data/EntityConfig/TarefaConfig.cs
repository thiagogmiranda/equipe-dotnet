﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TodoListApp.Domain.Entities;

namespace TodoListApp.Infra.Data.EntityConfig
{
	public class TarefaConfig : IEntityTypeConfiguration<Tarefa>
	{
		public void Configure(EntityTypeBuilder<Tarefa> builder)
		{
			builder.HasKey(t => t.Id);
			builder.Property(t => t.Nome).HasMaxLength(100);

			//builder.HasOne(t => t.Lista)
			//	.WithMany()
			//	.HasForeignKey(t => t.ListaId)
			//	.OnDelete(DeleteBehavior.NoAction);

			builder.HasOne(t => t.Usuario)
				.WithMany()
				.HasForeignKey(t => t.UsuarioId)
				.OnDelete(DeleteBehavior.NoAction);
		}
	}
}
