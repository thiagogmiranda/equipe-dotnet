﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TodoListApp.Domain.Entities;

namespace TodoListApp.Infra.Data.EntityConfig
{
	public class ListaConfig : IEntityTypeConfiguration<Lista>
	{
		public void Configure(EntityTypeBuilder<Lista> builder)
		{
			builder.HasKey(l => l.Id);
			builder.Property(l => l.Nome).HasMaxLength(100);

			builder.HasOne(l => l.Categoria)
				.WithMany()
				.HasForeignKey(l => l.CategoriaId)
				.OnDelete(DeleteBehavior.NoAction);

			builder.HasMany(l => l.Tarefas)
				.WithOne()
				.HasForeignKey(t => t.ListaId)
				.OnDelete(DeleteBehavior.Cascade);
		}
	}
}
