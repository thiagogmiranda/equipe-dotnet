﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TodoListApp.Domain.Entities;

namespace TodoListApp.Infra.Data.EntityConfig
{
	public class UsuarioConfig : IEntityTypeConfiguration<Usuario>
	{
		public void Configure(EntityTypeBuilder<Usuario> builder)
		{
			builder.HasKey(u => u.Id);
			builder.Property(p => p.Nome).HasMaxLength(100);
			builder.Property(p => p.Email).HasMaxLength(200);
			builder.Property(p => p.Senha);
		}
	}
}
