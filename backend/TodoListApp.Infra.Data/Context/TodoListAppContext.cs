﻿using Microsoft.EntityFrameworkCore;
using TodoListApp.Domain.Entities;

namespace TodoListApp.Infra.Data.Context
{
	public class TodoListAppContext : DbContext
	{
		public DbSet<Usuario> Usuarios { get; set; }
		public DbSet<Categoria> Categorias { get; set; }
		public DbSet<Lista> Listas { get; set; }
		public DbSet<Tarefa> Tarefas { get; set; }

		public TodoListAppContext(DbContextOptions<TodoListAppContext> options)
			: base(options)
		{

		}

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.ApplyConfigurationsFromAssembly(GetType().Assembly);
		}
	}
}
