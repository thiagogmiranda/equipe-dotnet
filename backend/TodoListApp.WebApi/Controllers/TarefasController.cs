﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using TodoListApp.Domain.Entities;
using TodoListApp.Domain.Interfaces.Services;

namespace TodoListApp.WebApi.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	[Authorize]
	public class TarefasController : ControllerBase
	{
		private readonly ITarefaService _tarefaService;

		public TarefasController(ITarefaService tarefaService)
		{
			_tarefaService = tarefaService;
		}

		[HttpGet]
		public IEnumerable<Tarefa> Get()
		{
			return _tarefaService.ObterTodos();
		}

		[HttpGet("lista/{id}")]
		public IEnumerable<Tarefa> ObterPorLista(int id)
		{
			return _tarefaService.ObterPorLista(id);
		}

		[HttpGet("{id}")]
		public ActionResult<Tarefa> Get(int id)
		{
			var lista = _tarefaService.ObterPorId(id);

			if(lista == null)
			{
				return NotFound();
			}

			return lista;
		}

		[HttpPost]
		public ActionResult Post([FromBody] Tarefa lista)
		{
			_tarefaService.Adicionar(lista);

			return Ok();
		}

		[HttpPut("{id}")]
		public ActionResult Put(int id, [FromBody] Tarefa lista)
		{
			if (id != lista.Id)
			{
				return BadRequest();
			}

			if (id == 0 || _tarefaService.ObterPorId(id) == null)
			{
				return NotFound();
			}

			_tarefaService.Atualizar(lista);

			return Ok();
		}

		[HttpDelete("{id}")]
		public ActionResult Delete(int id)
		{
			if (id == 0 || _tarefaService.ObterPorId(id) == null)
			{
				return NotFound();
			}

			_tarefaService.Remover(id);

			return Ok();
		}

		[HttpPut("concluir/{id}")]
		public ActionResult Concluir(int id)
		{
			var tarefa = _tarefaService.ObterPorId(id);

			if (tarefa == null)
			{
				return NotFound();
			}

			_tarefaService.Concluir(tarefa);

			return Ok();
		}
	}
}
