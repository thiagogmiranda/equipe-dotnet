﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TodoListApp.Domain.Entities;
using TodoListApp.Domain.Interfaces.Services;

namespace TodoListApp.WebApi.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	[Authorize]
	public class ListasController : ControllerBase
	{
		private readonly IListaService _listaService;

		public ListasController(IListaService listaService)
		{
			_listaService = listaService;
		}

		[HttpGet]
		public IEnumerable<Lista> Get(string nome, int idCategoria)
		{
			return _listaService.ListarPorNomeECategoria(nome, idCategoria);
		}

		[HttpGet("{id}")]
		public ActionResult<Lista> Get(int id)
		{
			var lista = _listaService.ObterPorId(id);

			if(lista == null)
			{
				return NotFound();
			}

			return lista;
		}

		[HttpPost]
		public ActionResult Post([FromBody] Lista lista)
		{
			_listaService.Adicionar(lista);

			return Ok();
		}

		[HttpPut("{id}")]
		public ActionResult Put(int id, [FromBody] Lista lista)
		{
			if (id == 0)
			{
				return NotFound();
			}

			lista.Id = id;

			_listaService.Atualizar(lista);

			return Ok();
		}

		[HttpDelete("{id}")]
		public ActionResult Delete(int id)
		{
			if (id == 0 || _listaService.ObterPorId(id) == null)
			{
				return NotFound();
			}

			_listaService.Remover(id);

			return Ok();
		}

		[HttpPut("concluir/{id}")]
		public ActionResult Concluir(int id)
		{
			var lista = _listaService.ObterPorId(id);

			if (lista == null)
			{
				return NotFound();
			}

			_listaService.Concluir(lista);

			return Ok();
		}
	}
}
