﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TodoListApp.Domain.Entities;
using TodoListApp.Domain.Interfaces.Services;

namespace TodoListApp.WebApi.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class UsuariosController : ControllerBase
	{
		private readonly IUsuarioService _usuarioService;

		public UsuariosController(IUsuarioService usuarioService)
		{
			_usuarioService = usuarioService;
		}

		// POST api/<UsuariosController>
		[HttpPost]
		[AllowAnonymous]
		public ActionResult Post([FromBody] Usuario usuario)
		{
			_usuarioService.Adicionar(usuario);

			return Ok();
		}

		[HttpPost]
		[AllowAnonymous]
		[Route("login")]
		public ActionResult Login([FromBody] Usuario usuario)
		{
			var user = _usuarioService.ObterParaLogin(usuario.Email, usuario.Senha);

			if (user == null)
			{
				return NotFound(new { message = "Usuário ou senha inválidos" });
			}

			// Gera o Token
			var token = TokenService.GenerateToken(user);

			// Retorna os dados
			return new JsonResult(
				new
				{
					id = user.Id,
					nome = user.Nome,
					senha = user.Senha,
					email = user.Email,
					token
				});
		}
	}
}
