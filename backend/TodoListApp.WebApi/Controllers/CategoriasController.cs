﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TodoListApp.Domain.Entities;
using TodoListApp.Domain.Interfaces.Services;

namespace TodoListApp.WebApi.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	[Authorize]
	public class CategoriasController : ControllerBase
	{
		private ICategoriaService _categoriaService;

		public CategoriasController(ICategoriaService categoriaService)
		{
			_categoriaService = categoriaService;
		}

		[HttpGet]
		public IEnumerable<Categoria> Get()
		{
			return _categoriaService.ObterTodos();
		}

		[HttpGet("{id}")]
		public ActionResult<Categoria> Get(int id)
		{
			var categoria = _categoriaService.ObterPorId(id);

			if (categoria == null)
			{
				return NotFound();
			}

			return categoria;
		}

		[HttpPost]
		public ActionResult Post([FromBody] Categoria categoria)
		{
			_categoriaService.Adicionar(categoria);

			return Ok();
		}

		[HttpPut("{id}")]
		public ActionResult Put([FromBody] Categoria categoria)
		{
			if (categoria.Id == 0) // || _categoriaService.ObterPorId(categoria.Id) == null)
			{
				return NotFound();
			}

			_categoriaService.Atualizar(categoria);

			return Ok();
		}

		[HttpDelete("{id}")]
		public ActionResult Delete(int id)
		{
			_categoriaService.Remover(id);

			return Ok();
		}
	}
}
