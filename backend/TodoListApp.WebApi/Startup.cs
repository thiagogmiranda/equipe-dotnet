using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TodoListApp.Domain.Interfaces.Repositories;
using TodoListApp.Domain.Interfaces.Services;
using TodoListApp.Domain.Services;
using TodoListApp.Infra.Data.Context;
using TodoListApp.Infra.Data.Repositories;

namespace TodoListApp.WebApi
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddCors(options =>
			{
				options.AddPolicy("EnableCORS", builder =>
				{
					builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod().Build();
				});
			});

			services.AddControllers();

			var key = Encoding.ASCII.GetBytes(TokenService.Secret);
			services.AddAuthentication(x =>
			{
				x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
				x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
			})
			.AddJwtBearer(x =>
			{
				x.RequireHttpsMetadata = false;
				x.SaveToken = true;
				x.TokenValidationParameters = new TokenValidationParameters
				{
					ValidateIssuerSigningKey = true,
					IssuerSigningKey = new SymmetricSecurityKey(key),
					ValidateIssuer = false,
					ValidateAudience = false
				};
			});

			services.AddDbContext<TodoListAppContext>(
				options => options.UseSqlServer(Configuration.GetConnectionString("TodoListApp")));

			services.AddTransient<IUsuarioService, UsuarioService>();
			services.AddTransient<ICategoriaService, CategoriaService>();
			services.AddTransient<IListaService, ListaService>();
			services.AddTransient<ITarefaService, TarefaService>();

			services.AddTransient<IUsuarioRepository, UsuarioRepository>();
			services.AddTransient<ICategoriaRepository, CategoriaRepository>();
			services.AddTransient<IListaRepository, ListaRepository>();
			services.AddTransient<ITarefaRepository, TarefaRepository>();
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}
			else
			{
				app.UseExceptionHandler("/error");
			}

			app.UseRouting();

			app.UseCors("EnableCORS");

			app.UseAuthentication();
			app.UseAuthorization();

			app.UseEndpoints(endpoints =>
			{
				endpoints.MapControllers();
			});
		}
	}
}
