﻿using System.Collections.Generic;
using TodoListApp.Domain.Entities;

namespace TodoListApp.Domain.Interfaces.Repositories
{
	public interface ITarefaRepository : IRepositoryBase<Tarefa>
	{
		IEnumerable<Tarefa> ObterPorLista(int idLista);
	}
}
