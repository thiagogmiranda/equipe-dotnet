﻿using TodoListApp.Domain.Entities;

namespace TodoListApp.Domain.Interfaces.Repositories
{
	public interface ICategoriaRepository : IRepositoryBase<Categoria>
	{
	}
}
