﻿using TodoListApp.Domain.Entities;

namespace TodoListApp.Domain.Interfaces.Repositories
{
	public interface IUsuarioRepository : IRepositoryBase<Usuario>
	{
		Usuario ObterParaLogin(string email, string senha);
	}
}
