﻿using System.Collections.Generic;

namespace TodoListApp.Domain.Interfaces.Repositories
{
	public interface IRepositoryBase<TEntidade> where TEntidade : class
	{
		void Adicionar(TEntidade entidade);
		void Atualizar(TEntidade entidade);
		TEntidade ObterPorId(int id);
		IEnumerable<TEntidade> ObterTodos();
		void Remover(int id);
		void Dispose();
	}
}
