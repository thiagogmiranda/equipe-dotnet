﻿using System.Collections.Generic;
using TodoListApp.Domain.Entities;

namespace TodoListApp.Domain.Interfaces.Repositories
{
	public interface IListaRepository : IRepositoryBase<Lista>
	{
		IEnumerable<Lista> ListarPorNomeECategoria(string nome, int idCategoria);
	}
}
