﻿using TodoListApp.Domain.Entities;

namespace TodoListApp.Domain.Interfaces.Services
{
	public interface IUsuarioService : IServiceBase<Usuario>
	{
		Usuario ObterParaLogin(string email, string senha);
	}
}
