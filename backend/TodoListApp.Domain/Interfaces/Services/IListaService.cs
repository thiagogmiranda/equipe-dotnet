﻿using System.Collections.Generic;
using TodoListApp.Domain.Entities;

namespace TodoListApp.Domain.Interfaces.Services
{
	public interface IListaService : IServiceBase<Lista>
	{
		void Concluir(Lista lista);
		IEnumerable<Lista> ListarPorNomeECategoria(string nome, int idCategoria);
	}
}
