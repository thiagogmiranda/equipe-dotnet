﻿using System.Collections.Generic;
using TodoListApp.Domain.Entities;

namespace TodoListApp.Domain.Interfaces.Services
{
	public interface ITarefaService : IServiceBase<Tarefa>
	{
		void Concluir(Tarefa tarefa);
		IEnumerable<Tarefa> ObterPorLista(int idLista);
	}
}
