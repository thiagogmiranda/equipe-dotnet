﻿using TodoListApp.Domain.Entities;

namespace TodoListApp.Domain.Interfaces.Services
{
	public interface ICategoriaService : IServiceBase<Categoria>
	{
	}
}
