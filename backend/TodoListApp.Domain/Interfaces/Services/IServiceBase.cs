﻿using System.Collections.Generic;

namespace TodoListApp.Domain.Interfaces.Services
{
	public interface IServiceBase<TEntidade> where TEntidade : class
	{
		void Adicionar(TEntidade entidade);
		void Atualizar(TEntidade entidade);
		TEntidade ObterPorId(int id);
		IEnumerable<TEntidade> ObterTodos();
		void Remover(int id);
		void Dispose();
	}
}
