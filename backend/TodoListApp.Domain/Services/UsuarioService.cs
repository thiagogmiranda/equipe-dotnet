﻿using TodoListApp.Domain.Entities;
using TodoListApp.Domain.Interfaces.Repositories;
using TodoListApp.Domain.Interfaces.Services;

namespace TodoListApp.Domain.Services
{
	public class UsuarioService : ServiceBase<Usuario>, IUsuarioService
	{
		private readonly IUsuarioRepository _usuarioRepository;

		public UsuarioService(IUsuarioRepository usuarioRepository)
			: base(usuarioRepository)
		{
			_usuarioRepository = usuarioRepository;
		}

		public Usuario ObterParaLogin(string email, string senha)
		{
			return _usuarioRepository.ObterParaLogin(email, senha);
		}
	}
}
