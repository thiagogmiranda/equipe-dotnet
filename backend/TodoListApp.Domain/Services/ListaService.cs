﻿using System.Collections.Generic;
using TodoListApp.Domain.Entities;
using TodoListApp.Domain.Interfaces.Repositories;
using TodoListApp.Domain.Interfaces.Services;

namespace TodoListApp.Domain.Services
{
	public class ListaService : ServiceBase<Lista>, IListaService
	{
		private readonly IListaRepository _listaRepository;

		public ListaService(IListaRepository listaRepository)
			: base(listaRepository)
		{
			_listaRepository = listaRepository;
		}

		public void Concluir(Lista lista)
		{
			lista.Concluir();

			_listaRepository.Atualizar(lista);
		}

		public IEnumerable<Lista> ListarPorNomeECategoria(string nome, int idCategoria)
		{
			return _listaRepository.ListarPorNomeECategoria(nome, idCategoria);
		}
	}
}
