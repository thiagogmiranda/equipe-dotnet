﻿using System;
using System.Collections.Generic;
using TodoListApp.Domain.Interfaces.Repositories;
using TodoListApp.Domain.Interfaces.Services;

namespace TodoListApp.Domain.Services
{
	public class ServiceBase<TEntidade> : IDisposable, IServiceBase<TEntidade> where TEntidade : class
	{
		protected readonly IRepositoryBase<TEntidade> _repository;

		public ServiceBase(IRepositoryBase<TEntidade> repository)
		{
			_repository = repository;
		}

		public void Adicionar(TEntidade entidade)
		{
			_repository.Adicionar(entidade);
		}

		public void Atualizar(TEntidade entidade)
		{
			_repository.Atualizar(entidade);
		}

		public TEntidade ObterPorId(int id)
		{
			return _repository.ObterPorId(id);
		}

		public void Remover(int id)
		{
			_repository.Remover(id);
		}

		public void Dispose()
		{
			_repository.Dispose();
		}

		public IEnumerable<TEntidade> ObterTodos()
		{
			return _repository.ObterTodos();
		}
	}
}
