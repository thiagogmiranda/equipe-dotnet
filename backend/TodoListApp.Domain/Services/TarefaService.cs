﻿using System.Collections.Generic;
using TodoListApp.Domain.Entities;
using TodoListApp.Domain.Interfaces.Repositories;
using TodoListApp.Domain.Interfaces.Services;

namespace TodoListApp.Domain.Services
{
	public class TarefaService : ServiceBase<Tarefa>, ITarefaService
	{
		private readonly ITarefaRepository _tarefaRepository;

		public TarefaService(ITarefaRepository tarefaRepository)
			: base(tarefaRepository)
		{
			_tarefaRepository = tarefaRepository;
		}

		public void Concluir(Tarefa tarefa)
		{
			tarefa.Concluir();

			_tarefaRepository.Atualizar(tarefa);
		}

		public IEnumerable<Tarefa> ObterPorLista(int idLista)
		{
			return _tarefaRepository.ObterPorLista(idLista);
		}
	}
}
