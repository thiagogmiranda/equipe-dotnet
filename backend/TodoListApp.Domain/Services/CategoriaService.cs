﻿using TodoListApp.Domain.Entities;
using TodoListApp.Domain.Interfaces.Repositories;
using TodoListApp.Domain.Interfaces.Services;

namespace TodoListApp.Domain.Services
{
	public class CategoriaService : ServiceBase<Categoria>, ICategoriaService
	{
		private readonly ICategoriaRepository _categoriaRepository;

		public CategoriaService(ICategoriaRepository categoriaRepository)
			: base(categoriaRepository)
		{
			_categoriaRepository = categoriaRepository;
		}
	}
}
