﻿namespace TodoListApp.Domain.Entities
{
	public class Tarefa
	{
		public int Id { get; set; }
		public string Nome { get; set; }
		public bool Concluida { get; set; }
		public int ListaId { get; set; }
		public int UsuarioId { get; set; }

		//public virtual Lista Lista { get; set; }
		public virtual Usuario Usuario { get; set; }

		public void Concluir()
		{
			Concluida = true;
		}
	}
}
