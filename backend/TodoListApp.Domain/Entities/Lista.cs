﻿using System.Collections.Generic;

namespace TodoListApp.Domain.Entities
{
	public class Lista
	{
		public int Id { get; set; }
		public string Nome { get; set; }
		public bool Concluida { get; set; }
		public int CategoriaId { get; set; }

		public virtual Categoria Categoria { get; set; }
		public virtual IEnumerable<Tarefa> Tarefas { get; set; }

		public void Concluir()
		{
			Concluida = true;

			//foreach (var item in Tarefas)
			//{
			//	item.Concluir();
			//}
		}
	}
}
