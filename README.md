# Prova da Equipe .NET

### Backend

Tecnologias utilizadas

- .NET Core 3.1
- .NET Core WebAPI
- Entity Framework Core
- JWT para autenticação


Utilizado banco de dados físico, desta forma é necessário executar os comandos do EF para criação da base.

Escolhi o .NET CORE por ser a plataforma que tenho maior contato e conhecimento.

### FrontEnd

Não havia tido contato com essas tecnologias então optei pelo Angular com Material.


A maior dificuldade foi a criação do frontend em Angular, pois foi meu primeiro contato.
